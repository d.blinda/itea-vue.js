import axios from 'axios'

const api = axios.create({
    baseURL: 'https://gorest.co.in/public-api',
    params: {
        'access-token' : 'F_XJDFvEGxupdn9P1d3HObnBGQdjw_l7EK4T',
        _format: 'json'
    }
});

const posts = {
    endpoint: '/posts',
    getPosts( params ) {
        let page = params || 1;
        return api.get(this.endpoint, {
            params: {
                page: page,
                fields: 'id,user_id,title'
            }
        })
    },
    getAuthorPosts( params, userId ) {
        let page = params || 1;
        return api.get(this.endpoint, {
            params: {
                page: page,
                user_id: userId,
                fields: 'id,user_id,title'
            }
        })
    },
    getPostById (postId, isFullPost) {
        return api.get(this.endpoint, {
            params: {
                id: postId,
                fields: isFullPost ? 'id,user_id,title,body' : 'body'
            }
        })
    }
};
const users = {
    endpoint: '/users',
    getUser (userId) {
        return api.get(this.endpoint, {
            params: {
                id: userId,
                fields: 'id,name,email,phone'
            }
        })
    }
};


export {posts, users}