// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '../App'
import router from '../router/index'
import VueMoment from 'vue-moment'
import Vuex from 'vuex';
import axios from 'axios'
import VueAxios from 'vue-axios'
import moment from 'moment-timezone'
import store from '../store/news'


Vue.config.productionTip = false;

Vue.use(VueMoment, { moment, });

Vue.use(Vuex);
Vue.use(VueAxios, axios);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
