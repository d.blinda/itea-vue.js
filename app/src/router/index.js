/* eslint-disable */

import Vue from 'vue'
import Router from 'vue-router'
import todo from '@/views/todo/todoIndex'
import home from '@/views/home'
import blog from '@/views/blog'
import author from '@/views/author'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: home
        },
        {
            path: '/todo',
            name: 'todo',
            component: todo,

        },

        {
            path: '/blog',
            name: 'blog',
            alias: '/blog/page-*',
            component: blog,

            children: [{
                path: '/blog/view/:id',
                name: 'viewNews',
                component: () => import('./../views/news.vue')
            }]
        },
        {
            path: '/author/:id',
            name: 'author',
            component: author,
        },
    ]
})
