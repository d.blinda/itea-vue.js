import { posts, users } from '../api/axios'

const state = {
    pageNews: [],
    currentPage: 1,
    pageCount: 0,
    user: {

    },
};
const mutations = {

    SET_NEWS_PREVIEWS(state, payload) {
        state.pageNews = payload;
        state.isLoaded = true;
    },
    SET_PAGE(state, payload) {
        state.currentPage = payload;
    },
    SET_COUNT_PAGE(state, payload) {
        state.pageCount = payload;
    },
    SET_USER(state,payload){
        state.user = payload
    }

};
const actions = {
    async INIT_AUTHOR_NEWS({dispatch}, authorID ) {
        await dispatch('GET_USER', authorID);
        await dispatch('GET_AUTHOR_NEWS', authorID);
    },
    async GET_AUTHOR_NEWS({commit, state}, authorID) {
        let {data} = await posts.getAuthorPosts(state.currentPage,authorID);
        console.log('authorID='+ authorID);
        console.log(data);
        commit('SET_NEWS_PREVIEWS', data.result);
        commit('SET_PAGE', data['_meta'].currentPage);
        commit('SET_COUNT_PAGE', data['_meta'].pageCount);
    },
    async GET_USER({commit, state}, authorID) {
        let {data} = await users.getUser(authorID);
        commit('SET_USER', data.result[0]);
    },
};
const getters = {
    GET_AUTHOR_NEWS_A (state) {
        return state.pageNews
    },
};


export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}