import Vue from 'vue';
import Vuex from 'vuex';
import { posts } from '../api/axios'
import news from './news'
import authors from './author'

Vue.use(Vuex);

export default new Vuex.Store({
    modules:{
        news,
        authors
    },
    state: {
        pageNews: [],
        currentPage: 1,
        pageCount: 0,
        isLoaded: false
    },
    mutations: {
        SET_NEWS_PREVIEWS(state, payload) {
            state.pageNews = payload;
            state.isLoaded = true;
        },
        SET_PAGE(state, payload) {
            state.currentPage = payload;
        },
        SET_COUNT_PAGE(state, payload) {
            state.pageCount = payload;
        }
    },
    actions: {
        async INIT_NEWS_PREVIEWS({commit, state}, qParams){
            let {data} = await posts.getPosts(qParams);
            commit('SET_NEWS_PREVIEWS', data.result);
            commit('SET_PAGE', data['_meta'].currentPage);
            commit('SET_COUNT_PAGE', data['_meta'].pageCount);
        },
    },
    getters: {
        GET_NEWS(state) {
            return state.pageNews
        },
        GET_CURRENT_PAGE(state) {
            return state.currentPage
        },
        GET_CURRENT_POST: state => postId => {
            return state.pageNews.filter(news => news.id === postId)
        }
    },

})