import { posts, users } from '../api/axios'

const state = {
    news: {
        id: null,
        user_id: null,
        title: null,
        body: null,
        isFull: false,
        loading: false
    },
    user: {

    },
};
const mutations = {
    SET_NEWS_ID(state, payload) {
        state.news.id = payload
    },
    SET_POST_STATE(state, payload) {
        state.news.isFull = payload
    },
    SET_POST_LOADING(state, payload) {
        state.news.loading = payload
    },
    SET_BASE_NEWS(state, data) {
        for (let key in data) {
            if (data.hasOwnProperty(key) && state.news.hasOwnProperty(key)) {
                state.news[key] = data[key]
            }
        }
    },
    SET_USER(state,payload){
        state.user = payload
    }

};
const actions = {
    async INIT_NEWS({dispatch}, newsID ) {
        await dispatch('GET_CURRENT_NEWS', newsID);
        await dispatch('GET_NEWS');
        await dispatch('GET_USER');
    },
    async GET_NEWS({commit, state}) {
        let {data} = await posts.getPostById(state.news.id, state.news.isFull);
        commit('SET_BASE_NEWS', data.result[0]);
        commit('SET_POST_LOADING', true);
    },
    async GET_USER({commit, state}) {
        let {data} = await users.getUser(state.news.user_id);
        commit('SET_USER', data.result[0]);
    },
    async GET_CURRENT_NEWS({dispatch, commit ,state, rootGetters}, postID ) {
        commit ('SET_NEWS_ID',postID);
        let post = rootGetters.GET_CURRENT_POST(postID);
        if(post.length !== 0) {
            commit('SET_POST_STATE', false);
            commit('SET_BASE_NEWS', post[0]);
        } else {
            commit('SET_POST_STATE', true);
        }
    }

};
const getters = {

};


export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}