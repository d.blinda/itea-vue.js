module.exports = {
    chainWebpack: config => {
        config.module
            .rule('sass')
            .oneOf('vue')
            .use('resolve-url-loader')
            .loader('resolve-url-loader').options({
            keepQuery: true
        })
            .before('sass-loader');
    }
}